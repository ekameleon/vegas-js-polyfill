# VEGAS JS

**Vegas JS Polyfill** - **version 1.0.8** is an *Opensource* Library based on **ECMAScript** for develop crossplatform **Rich Internet Applications** and **Games**.

This library contains a set of polyfills modules writing in **Javascript** and based on the [ES6](http://es6-features.org/) standard.
### About

 * Author : Marc ALCARAZ (aka eKameleon) - Creative Technologist and Digital Architect
 * Mail : ekameleon[at]gmail.com
 * LinkedIn : [https://www.linkedin.com/in/ekameleon/](https://www.linkedin.com/in/ekameleon/)

### License

Under tree opensource licenses :

 * [License MPL 2.0](https://www.mozilla.org/en-US/MPL/2.0/)
 * [License GPL 2.0+](https://www.gnu.org/licenses/gpl-2.0.html)
 * [License LGPL 2.1+](https://www.gnu.org/licenses/lgpl-2.1.html)

## Resources

#### ⌜ Download

Download on **Bitbucket** the latest code, report an issue, ask a question or contribute :

 * [https://bitbucket.org/ekameleon/vegas-js-polyfill](https://bitbucket.org/ekameleon/vegas-js-polyfill)

#### ⌜ Documentation

Get started with the the **Vegas JS** API :

 * [https://vegasjs.ooopener.com/](https://vegasjs.ooopener.com/)

#### ⌜ Slack Community

![slack-logo-vector-download.jpg](https://bitbucket.org/repo/AEbB9b/images/3509366499-slack-logo-vector-download.jpg)

Send us your email to join the **VEGAS** community on Slack !

## Install

#### ⌜ YARN / NPM

You can install VEGAS JS with [NPM](https://www.npmjs.com/package/vegas-js) or [Yarn](https://yarnpkg.com/).

```
$ yarn add vegas-js-polyfill --dev
```

or

```
$ npm install vegas-js-polyfill --save-dev
```

## Building and test the libraries

**VEGAS JS CORE** use [Yarn](https://yarnpkg.com/) with a serie of powerful packages (Babel, Mocha, etc.) to compile and build this library.

#### ⌜ Simple Build

1 - The first time, initialize the project and run yarn :
```
$ yarn
```

2 - Test + compile all the libraries :
```
$ yarn build
```

#### ⌜ VEGAS JS Polyfill Build

1 - Build the **./dist/vegas-polyfill.js** : not minified + no comments + sourcemap.
```
$ yarn dev
```

2 - Build the **./dist/vegas-polyfill.js** and watch the changing into the **./src** folder.
```
$ yarn watch
```

3 - Build the **./dist/vegas-polyfill.min.js** : minified + no comments.
```
$ yarn prod
```

4 - Build the **./dist/vegas-core.polyfill.js** and the **./dist/vegas-polyfill.js** libraries only.
```
$ yarn core
```

#### ⌜ Unit tests

We use the [Mocha](https://mochajs.org) and the Chai (http://chaijs.com/) tools to run the unit tests of the VEGAS JS libraries.

Run all unit tests
```
$ yarn test
```
 