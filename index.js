"use strict" ;

import './src/Date'
import './src/Function'
import './src/Math'
import './src/Object'
import './src/Uint32Array'

import { global } from './src/global'

import performance from './src/performance'

export { cancelAnimationFrame  } from './src/requestAnimationFrame'
export { requestAnimationFrame } from './src/requestAnimationFrame'

Object.defineProperty( exports , '__esModule', { value: true });

exports.global       = global ;
exports.performance  = performance ;
