"use strict" ;

if ( Object.assign === undefined )
{
    // Missing in IE.
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
    ( function () {

        Object.assign = function ( target )
        {
            if ( target === undefined || target === null )
            {
                throw new TypeError( 'Cannot convert undefined or null to object' );
            }

            let output = Object( target );

            for ( let index = 1; index < arguments.length; index ++ )
            {
                let source = arguments[ index ];
                if ( source !== undefined && source !== null )
                {
                    for ( let nextKey in source )
                    {
                        if ( Object.prototype.hasOwnProperty.call( source, nextKey ) )
                        {
                            output[ nextKey ] = source[ nextKey ];
                        }
                    }
                }
            }
            return output;
        };
    } )();
}