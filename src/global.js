"use strict" ;


let object ;

try
{
    // noinspection JSAnnotator
    object = global || null ;
}
catch( ignore )
{

}

if( !object )
{
    try
    {
        // noinspection JSAnnotator
        object = window ;
    }
    catch( e )
    {

    }
}

if( !object )
{
    try
    {
        // noinspection JSAnnotator
        object = document ;
    }
    catch( ignore )
    {

    }
}

if( !object )
{
    // noinspection JSAnnotator
    object = {} ;
}


/**
 * The <code>global</code> namespace (reference to the global scope of the application), this object can target the <code>window</code> or the <code>document</code> global objects in the browser or an other global reference.
 * @name global
 * @memberof core
 * @const
 * @instance
 */
export var global = object ;