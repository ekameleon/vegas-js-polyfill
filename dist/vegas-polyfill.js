/**
 * A specialized polyfill javascript library. - version: 1.0.8 - license: MPL 2.0/GPL 2.0+/LGPL 2.1+ - Follow me on Twitter! @ekameleon
 */

(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
	typeof define === 'function' && define.amd ? define(['exports'], factory) :
	(factory((global.polyfill = {})));
}(this, (function (exports) { 'use strict';

if (!(Date.now && Date.prototype.getTime)) {
    Date.now = function now() {
        return new Date().getTime();
    };
}

if (!Function.prototype.bind) {
    Function.prototype.bind = function (oThis) {
        if (typeof this !== "function") {
            throw new TypeError('Function.prototype.bind called on incompatible ' + this);
        }
        var aArgs = Array.prototype.slice.call(arguments, 1),
            fToBind = this,
            fNOP = function fNOP() {},
            fBound = function fBound() {
            return fToBind.apply(this instanceof fNOP ? this : oThis, aArgs.concat(Array.prototype.slice.call(arguments)));
        };
        if (this.prototype) {
            fNOP.prototype = this.prototype;
        }
        fBound.prototype = new fNOP();
        return fBound;
    };
}
if (Function.prototype.name === undefined) {
    Object.defineProperty(Function.prototype, 'name', {
        get: function get() {
            return this.toString().match(/^\s*function\s*(\S*)\s*\(/)[1];
        }
    });
}

if (Math.sign === undefined) {
    Math.sign = function (x) {
        return x < 0 ? -1 : x > 0 ? 1 : +x;
    };
}

if (Object.assign === undefined) {
    (function () {
        Object.assign = function (target) {
            if (target === undefined || target === null) {
                throw new TypeError('Cannot convert undefined or null to object');
            }
            var output = Object(target);
            for (var index = 1; index < arguments.length; index++) {
                var source = arguments[index];
                if (source !== undefined && source !== null) {
                    for (var nextKey in source) {
                        if (Object.prototype.hasOwnProperty.call(source, nextKey)) {
                            output[nextKey] = source[nextKey];
                        }
                    }
                }
            }
            return output;
        };
    })();
}

(function (global) {
    if (typeof global.Uint32Array !== "function") {
        var CheapArray = function CheapArray(type) {
            var proto = [];
            global[type] = function (arg) {
                var i = void 0;
                if (typeof arg === "number") {
                    Array.call(this, arg);
                    this.length = arg;
                    for (i = 0; i < this.length; i++) {
                        this[i] = 0;
                    }
                } else {
                    Array.call(this, arg.length);
                    this.length = arg.length;
                    for (i = 0; i < this.length; i++) {
                        this[i] = arg[i];
                    }
                }
            };
            global[type].prototype = proto;
            global[type].constructor = global[type];
        };
        CheapArray('Float32Array');
        CheapArray('Uint32Array');
        CheapArray('Uint16Array');
        CheapArray('Int16Array');
        CheapArray('ArrayBuffer');
    }
})(window || global || {});

var object = void 0;
try {
    object = global$1 || null;
} catch (ignore) {}
if (!object) {
    try {
        object = window;
    } catch (e) {}
}
if (!object) {
    try {
        object = document;
    } catch (ignore) {}
}
if (!object) {
    object = {};
}
var global$1 = object;

var performance = global$1.performance || {};
Object.defineProperty(global$1, 'performance', { value: performance, configurable: true, writable: true });
performance.now = performance.now || performance.mozNow || performance.msNow || performance.oNow || performance.webkitNow;
if (!(global$1.performance && global$1.performance.now)) {
                  var startTime = Date.now();
                  global$1.performance.now = function () {
                                    return Date.now() - startTime;
                  };
}

var ONE_FRAME_TIME = 16;
var lastTime = Date.now();
var vendors = ['ms', 'moz', 'webkit', 'o'];
var len = vendors.length;
for (var x = 0; x < len && !global$1.requestAnimationFrame; ++x) {
    var p = vendors[x];
    global$1.requestAnimationFrame = global$1[p + 'RequestAnimationFrame'];
    global$1.cancelAnimationFrame = global$1[p + 'CancelAnimationFrame'] || global$1[p + 'CancelRequestAnimationFrame'];
}
if (!global$1.requestAnimationFrame) {
    global$1.requestAnimationFrame = function (callback) {
        if (typeof callback !== 'function') {
            throw new TypeError(callback + 'is not a function');
        }
        var currentTime = Date.now();
        var delay = ONE_FRAME_TIME + lastTime - currentTime;
        if (delay < 0) {
            delay = 0;
        }
        lastTime = currentTime;
        return setTimeout(function () {
            lastTime = Date.now();
            callback(performance.now());
        }, delay);
    };
}
if (!global$1.cancelAnimationFrame) {
    global$1.cancelAnimationFrame = function (id) {
        return clearTimeout(id);
    };
}
var cancelAnimationFrame = global$1.cancelAnimationFrame;
var requestAnimationFrame = global$1.requestAnimationFrame;

Object.defineProperty(exports, '__esModule', { value: true });
exports.global = global$1;
exports.performance = performance;

exports.cancelAnimationFrame = cancelAnimationFrame;
exports.requestAnimationFrame = requestAnimationFrame;

Object.defineProperty(exports, '__esModule', { value: true });

})));
